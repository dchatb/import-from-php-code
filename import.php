<?php

if(isset($_POST['Submit']))
{
  $ckey="";
  //validate whether uploaded file is a csv file
  $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
  if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes))
  {
    if(is_uploaded_file($_FILES['file']['tmp_name']))
    {
      //open uploaded csv file with read only mode
      ini_set('auto_detect_line_endings',TRUE);
      $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
      $datetime = date("Y-m-d H:i:s");
      $date = date("Y-m-d", strtotime($datetime));
      $file_created_by = "import_script";
      $filename = 'import_input_'.date("YmdHis", strtotime($datetime)).'_'.$file_created_by.'.csv';

      $upload_filepath = 'D:/xampp/htdocs/import/uploaded_csv_files/';
      if(!is_dir($upload_filepath))
      {
        mkdir($upload_filepath, 0777, true);
        shell_exec("chmod -R 777 $upload_filepath");
      }

      $upload_filepath_error = $upload_filepath.'error/';
      if(!is_dir($upload_filepath_error))
      {
        mkdir($upload_filepath_error, 0777, true);
        shell_exec("chmod -R 777 $upload_filepath_error");
      }

      $upload_filepath_processed = $upload_filepath.'success/';
      if(!is_dir($upload_filepath_processed))
      {
        mkdir($upload_filepath_processed, 0777, true);
        shell_exec("chmod -R 777 $upload_filepath_processed");
      }

      $upload_filepath_error .= $date.'/';
      $upload_filepath_processed .= $date.'/';

      if(!is_dir($upload_filepath_error))
      {
        mkdir($upload_filepath_error, 0777, true);
        shell_exec("chmod -R 777 $upload_filepath_error");
      }

      if(!is_dir($upload_filepath_processed))
      {
        mkdir($upload_filepath_processed, 0777, true);
        shell_exec("chmod -R 777 $upload_filepath_processed");
      }

      $fp = @fopen($upload_filepath_processed.$filename, 'w');
      $fe = @fopen($upload_filepath_error.$filename, 'w');
      //parse data from csv file line by line
      $i=0;
      $s=0;
      $rejected=0;
      $accepted=0;
      $total=0;
      //skip first line
      $header = fgetcsv($csvFile);
      $temp_header = $header;
      array_unshift($header,'id customer');
      array_push($temp_header,'Reject Reason');
      array_push($temp_header,'Reject Error');

      fputcsv($fp,$header,',');
      fputcsv($fe,$temp_header,',');

      //parse data from csv file line by line
      while(($line = fgetcsv($csvFile)) !== FALSE)
      {
		if(empty($line))
			continue;
        $i++;
        $error = false;
        $error_input = false;
        $error_input_msg = array();

        $fname = strtoupper(filter_var($line[0],FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH));
        if(empty($fname))
        {
          $error_input = true;
          $error_input_msg[] = "First Name Blank";
        }
        else if(!preg_match("/^[a-zA-Z]'?([a-zA-Z]|\.| |-)+$/",$fname))
        {
          $error_input = true;
          $error_input_msg[] = "Enter A Valid First Name";
        }

        $lname = strtoupper(filter_var($line[1],FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH));
        if(empty($lname))
        {
          $error_input = true;
          $error_input_msg[] = "Last Name Blank";
        }
        else if(!preg_match("/^[a-zA-Z]'?([a-zA-Z]|\.| |-)+$/",$lname))
        {
          $error_input = true;
          $error_input_msg[] = "Enter A Valid Last Name";
        }

        $email = filter_var($line[2],FILTER_SANITIZE_EMAIL);
		if(empty($email))
        {
          $error_input = true;
          $error_input_msg[] = "Email is Blank";
        }
		else
		{
			$email_exists = "select email from import_sample_table where email='".$email."'";
			if ($email_result=mysqli_query($con,$email_exists))
				{
				  // Return the number of rows in result set
				  $email_count=mysqli_num_rows($email_result);
				  mysqli_free_result($email_result);
				  if($email_count>0)
				  {
					$error_input = true;
					$error_input_msg[] = "Email is already present in the database!";
				  }
				 }
			
		}
		
		$mobile = filter_var($line[3],FILTER_SANITIZE_NUMBER_INT);
        if(empty($mobile))
        {
          $error_input = true;
          $error_input_msg[] = "Mobile Number is Blank";
        }
        else
        {
          if(!preg_match('/^[6-9][0-9]{9}$/',$mobile))
          {
            $error_input = true;
            $error_input_msg[] = "Mobile Number format is wrong.";
          }
		  else		
		 {
		   $mobile_exists = "select mobile from import_sample_table where mobile='".$mobile."'";
		   if($mobile_result=mysqli_query($con,$mobile_exists))
			 {
			  // Return the number of rows in result set
			  $mobile_count=mysqli_num_rows($mobile_result);
				  mysqli_free_result($mobile_result);
				  if($mobile_count>0)
				  {
					$error_input = true;
					$error_input_msg[] = "Mobile is already present in the database!";
				  }
				 }
			
			}
		  
        }
		
		$pincode = filter_var($line[4],FILTER_SANITIZE_NUMBER_INT);
         if(!preg_match('/^[1-9][0-9]{5}$/',$pincode))
         {
           $error_input = true;
           $error_input_msg[] = "Invalid PIN Code";
         }		
		
		
		$state = strtoupper(filter_var($line[5],FILTER_SANITIZE_STRING));
        if(empty($state))
        {
          $error_input = true;
          $error_input_msg[] = "State is Blank";
        }
		
		$district = strtoupper(filter_var($line[6],FILTER_SANITIZE_STRING));	
        if(empty($district))
        {
          $error_input = true;
          $error_input_msg[] = "District is Blank";
        }

		$city = strtoupper(filter_var($line[7],FILTER_SANITIZE_STRING));    
		if(empty($city))
        {
          $error_input = true;
          $error_input_msg[] = "City is Blank";
        }

        $gender = strtoupper(filter_var($line[8],FILTER_SANITIZE_STRING));
        
		if(empty($gender))
        {
          $error_input = true;
          $error_input_msg[] = "Gender is Blank";
        }
		else
		{
		 if(!strcasecmp($gender,'MALE'))
			$gender='MALE';
			elseif(!strcasecmp($gender,'FEMALE'))
			$gender='FEMALE';
			elseif(!strcasecmp($gender,'OTHERS'))
			$gender='OTHERS';
		 else
			{
			$gender = '';
			$error_input = true;
			$error_input_msg[] = "Gender is Wrong! It should be MALE, FEMALE OR OTHERS!";
			}
		}


        //insert member data into database

        if($error_input)
        {
		  $rejected++;
          array_push($line,implode(" / ",$error_input_msg));
          fputcsv($fe,$line,',');
          continue;
        }



		//die("herex");
		
          date_default_timezone_set('Asia/Kolkata');
          $createdby ="Import Script";
          $new_import_rowid = 0;
          $insImport = "INSERT INTO import_sample_table
          (first_name,last_name,email,mobile,pincode,state,district,city,gender,createdby) 
		  VALUES
		  ('".$fname."','".$lname."','".$email."','".$mobile."','".$pincode."','".$state."','".$district."','".$city."','".$gender."','".$createdby."')";
		  
		  $res_import = mysqli_query($con,$insImport);
          if(mysqli_affected_rows($con)>0)
          {            
            $new_import_rowid = mysqli_insert_id($con);
		  }
		  else
		  {
			  $error= true;
			  $error_msg="Unable to import";
		  }
            
		//die("here");
		
		
        if(!$error)
        {
          $s++;
          $accepted++;
          array_unshift($line,$new_import_rowid);
          fputcsv($fp,$line,',');

        }
        else
        {
          $rejected++;
          array_push($line,$error_msg);
          array_push($line,$error_msg_log);
          fputcsv($fe,$line,',');

        }
      }

      //close opened csv file
      ini_set('auto_detect_line_endings',FALSE);
      fclose($fp);
      fclose($fe);
      fclose($csvFile);
      //
	  
	  $total=$accepted+$rejected;
	  $accepted_percentage=round(($accepted/$total)*100);
	  $rejected_percentage=round(($rejected/$total)*100);
	  
	  echo "Accepted ".$accepted."/".$total."<br>";
	  echo "Rejected ".$rejected."/".$total."<br>";
	  
	  $created_by = 'Import Script';
	  $importedby= 'DC';
		  
	  $bulk_info_sql="insert into bulk_import (accepted, rejected, total, accepted_percentage, rejected_percentage,processed_file_path,error_file_path,filename,importedby,createdby)
					  values ($accepted,$rejected,$total,$accepted_percentage,$rejected_percentage,'".$upload_filepath_processed."','".$upload_filepath_error."','".$filename."','".$importedby."','".$created_by."')";
	  $bulk_info_sql_res=mysqli_query($con,$bulk_info_sql);
	  
      if($i==$s)
      {
        unlink($upload_filepath_error.$filename);
        if(count(glob($upload_filepath_error.'*.*'))==0)
        rmdir($upload_filepath_error);
        ?>
        <div id="messages1" class="alert alert-success alert-dismissable" role="alert">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <div id="messages_content1">Successfully Uploaded the File.</div>
          <a href="uploaded_csv_files/success/<?php echo $date.'/'.$filename;?>" style="color:blue;text-weight:bold;" target="_blank" download>Click Here to download the processed file</a>
        </div>
        <?php
      }
      elseif($s==0 && $i!=0)
      {
        unlink($upload_filepath_processed.$filename);
        if(count(glob($upload_filepath_processed.'*.*'))==0)
        rmdir($upload_filepath_processed);
        ?>
        <div id="messages1" class="alert alert-warning alert-dismissable" role="alert">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <div id="messages_content1">Some lines couldn't able to update. Please check the error file and upload again.</div>
          <a href="uploaded_csv_files/error/<?php echo $date.'/'.$filename;?>" style="color:blue;text-weight:bold;" target="_blank" download>Click Here to download the error log</a>
        </div>
        <?php
      }
      else
      {
        ?>
        <div id="messages1" class="alert alert-success alert-dismissable" role="alert">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <div id="messages_content1">Successfully Uploaded the File.</div>
          <a href="uploaded_csv_files/success/<?php echo $date.'/'.$filename;?>" style="color:blue;text-weight:bold;" target="_blank" download>Click Here to download the processed file</a>
        </div>
        <div id="messages2" class="alert alert-warning alert-dismissable" role="alert">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <div id="messages_content2">Some lines couldn't able to update. Please check the error file and upload again.</div>
          <a href="uploaded_csv_files/error/<?php echo $date.'/'.$filename;?>" style="color:blue;text-weight:bold;" target="_blank" download>Click Here to download the error log</a>
        </div>
        <?php
      }
    }
    else
    {
      ?>
      <div id="messages1" class="alert alert-danger alert-dismissable" role="alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <div id="messages_content1">Something went wrong. Please Upload the file again.</div>
      </div>
      <?php
    }
  }
  else
  {
    ?>
    <div id="messages1" class="alert alert-danger alert-dismissable" role="alert">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <div id="messages_content1">Invalid File Format. Please Upload the correct file again or download the template.</div>
    </div>
    <?php
  }
}

?>
