-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 06, 2019 at 02:46 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `import`
--

-- --------------------------------------------------------

--
-- Table structure for table `bulk_import`
--

CREATE TABLE `bulk_import` (
  `rowid` int(11) NOT NULL,
  `accepted` int(11) NOT NULL,
  `rejected` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `accepted_percentage` decimal(10,2) NOT NULL,
  `rejected_percentage` decimal(10,2) NOT NULL,
  `processed_file_path` varchar(100) NOT NULL,
  `error_file_path` varchar(100) NOT NULL,
  `filename` varchar(100) NOT NULL,
  `importedby` varchar(100) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bulk_import`
--

INSERT INTO `bulk_import` (`rowid`, `accepted`, `rejected`, `total`, `accepted_percentage`, `rejected_percentage`, `processed_file_path`, `error_file_path`, `filename`, `importedby`, `createdon`, `createdby`) VALUES
(1, 4, 0, 4, '100.00', '0.00', 'D:/xampp/htdocs/import/uploaded_csv_files/success/2019-08-06/', 'D:/xampp/htdocs/import/uploaded_csv_files/error/2019-08-06/', 'import_input_20190806143033_import_script.csv', 'DC', '2019-08-06 12:30:34', 'Import Script');

-- --------------------------------------------------------

--
-- Table structure for table `import_sample_table`
--

CREATE TABLE `import_sample_table` (
  `rowid` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `pincode` varchar(6) NOT NULL,
  `state` varchar(50) NOT NULL,
  `district` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `createdby` varchar(50) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `import_sample_table`
--

INSERT INTO `import_sample_table` (`rowid`, `first_name`, `last_name`, `email`, `mobile`, `pincode`, `state`, `district`, `city`, `gender`, `createdby`, `createdon`) VALUES
(1, 'DIPAK', 'CHATTERJEE', 'dchatb@gmail.com', '7003376429', '700050', 'WEST BENGAL', 'NORT 24 PARGANAS', 'KOLKATA', 'MALE', 'Import Script', '2019-08-06 12:30:34'),
(2, 'UJJAL', 'MODAK', 'ujjalai@jobiak.ai.com', '6287988178', '768899', 'ANDHRAPRADESH', 'VIZAG', 'VIZAG', 'FEMALE', 'Import Script', '2019-08-06 12:30:34'),
(3, 'UJJAL', 'MODAK', 'ujjalai@jobiak.ai.com1', '6287988170', '768899', 'ANDHRAPRADESH', 'VIZAG', 'VIZAG', 'FEMALE', 'Import Script', '2019-08-06 12:30:34'),
(4, 'UJJAL', 'MODAK', 'ujjalai@jobiak.ai.com2', '6287988171', '768899', 'ANDHRAPRADESH', 'VIZAG', 'VIZAG', 'FEMALE', 'Import Script', '2019-08-06 12:30:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bulk_import`
--
ALTER TABLE `bulk_import`
  ADD PRIMARY KEY (`rowid`);

--
-- Indexes for table `import_sample_table`
--
ALTER TABLE `import_sample_table`
  ADD PRIMARY KEY (`rowid`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `mobile` (`mobile`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bulk_import`
--
ALTER TABLE `bulk_import`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `import_sample_table`
--
ALTER TABLE `import_sample_table`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
